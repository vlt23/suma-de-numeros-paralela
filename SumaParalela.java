/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import es.urjc.etsii.code.concurrency.SimpleConcurrent;
import es.urjc.etsii.code.concurrency.SimpleSemaphore;

import java.util.Arrays;


public class SumaParalela {

    private static final int N_PROCESOS = 5;
    private static final int N_DATOS = 16;
    private static final int N_RESULTADOS = N_DATOS / 2;
    private static int tope = N_DATOS;
    private static volatile int posicionActual = 0;
    private static volatile int hilosTerm = 0;

    private static int[] datos = new int[N_DATOS];
    private static int[] resultados = new int[N_RESULTADOS];

    private static SimpleSemaphore emHilos;
    private static SimpleSemaphore emNivel;
    private static SimpleSemaphore sb;
    private static SimpleSemaphore desbloqueo;


    private static int suma(int a, int b) {
        SimpleConcurrent.sleepRandom(1000);
        return a + b;
    }

    public static void inicializaDatos() {
        for (int i = 0; i < N_DATOS; i++) {
            datos[i] = (int) (Math.random() * 11);
        }
        SimpleConcurrent.println("Los datos a sumar son: " + Arrays.toString(datos));
    }

    private static void sumaNumerosParalela(int numprocesoActual) {
        while (posicionActual < tope) {
            emHilos.acquire();
            int posicion = posicionActual;
            posicionActual = posicionActual + 2;
            emHilos.release();

            infoInicio(numprocesoActual, posicion);
            int result = suma(datos[posicion], datos[posicion + 1]);
            if (posicion == 0) {
                infoGuardar(numprocesoActual, posicion, result);
                resultados[0] = result;
            } else {
                posicion = posicion / 2;
                infoGuardar(numprocesoActual, posicion, result);
                resultados[posicion] = result;
            }
        }
    }

    private static void infoInicio(int proceso, int posicion) {
        SimpleConcurrent.println("Proceso " + proceso + ": Se inicia la suma de datos[" + posicion + "] = " +
                datos[posicion] + " y datos[" + (posicion + 1) + "] = " + datos[posicion + 1]);
    }

    private static void infoGuardar(int proceso, int posicion, int result) {
        SimpleConcurrent.println(proceso + ": Se guarda la suma en resultados[" + posicion + "] = " + result);
    }

    public static void calculo(int numProcesoActual) {
        int niveles = N_DATOS / 4;
        for (int nivelActual = 0; nivelActual < niveles; nivelActual++) {
            sumaNumerosParalela(numProcesoActual);
            emNivel.acquire();
            hilosTerm++;
            if (hilosTerm < N_PROCESOS) {
                emNivel.release();
                sb.acquire();
                desbloqueo.release();
            } else {
                tope = tope / 2;
                imprimirResultados(numProcesoActual);
                for (int i = 0; i < tope; i++) {
                    datos[i] = resultados[i];
                }
                SimpleConcurrent.println("Finalizado el nivel " + (nivelActual + 1));
                SimpleConcurrent.println("------------------------------------------------------------------------");
                hilosTerm = 0;
                posicionActual = 0;
                sb.release(N_PROCESOS - 1);
                desbloqueo.acquire(N_PROCESOS - 1);
                emNivel.release();

                if (nivelActual + 1 == niveles) {
                    SimpleConcurrent.println("El resultado es: " + resultados[0]);
                }
            }
        }
    }

    private static void imprimirResultados(int numProcesoActual) {
        SimpleConcurrent.print("Proceso " + numProcesoActual + ": Actualizando el array de datos a [ ");
        for (int i = 0; i < tope; i++) {
            SimpleConcurrent.print(resultados[i] + " ");
        }
        SimpleConcurrent.println("]");
    }

    public static void main(String[] args) {
        emNivel = new SimpleSemaphore(1);
        emHilos = new SimpleSemaphore(1);
        sb = new SimpleSemaphore(0);
        desbloqueo = new SimpleSemaphore(0);

        inicializaDatos();
        for (int i = 0; i < N_PROCESOS; i++) {
            SimpleConcurrent.createThread("calculo", i);
        }
        SimpleConcurrent.startThreadsAndWait();
    }

}
